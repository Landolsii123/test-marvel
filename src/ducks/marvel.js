import { getCharacters, getOneCharacter } from "../services/marvelService";

export const GET_CHARACTERS = 'GET_CHARACTERS';
export const GET_ONE_CHARACTER = 'GET_ONE_CHARACTER';

export const getAllCharacters = () => dispatch => {
    getCharacters().then(characters=>{
        dispatch({
            type: GET_CHARACTERS,
            characters:characters.data.results
        });
    })
};

export const getOnlyOneCharacter = id => dispatch => {
    getOneCharacter(id).then(character=>{
        console.log("m duckssss",character);
        dispatch({
            type: GET_ONE_CHARACTER,
            character:character.data.results[0]
        });
    })
};

const actions = { getAllCharacters, getOnlyOneCharacter };

const intialState = {
    characters:[],
    character:null
};

const reducer = (state = intialState, action) => {
    switch (action.type) {
      case GET_CHARACTERS:
        return {
          ...state,
          characters: action.characters,
        };
        case GET_ONE_CHARACTER:
        return {
          ...state,
          character: action.character,
        };

      default:
        return state;
    }
};
export default { actions, reducer };