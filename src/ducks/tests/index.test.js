import reducers from '../../reducers';
import actions from '../../actions';
import axiosMock from 'axios';
import { configureStore } from '../../../middleware';
import { GET_CHARACTERS, GET_ONE_CHARACTER } from '../marvel';
import { getCharacter, getCharacters } from '../../selectors/marvel';
jest.mock('axios', () => ({
  get: jest.fn(),
}));
describe('Ducks | Marvel', () => {
  const data = { data: { results: [{}] } };
  axiosMock.get.mockResolvedValue({ data: data });
  it('should get characters', async () => {
    const initState = {
      marvel: {
        characters: [],
      },
    };
    const hook = {
      [GET_CHARACTERS]: getState => {
        const characters = getCharacters(getState());
        expect(characters.length).toEqual(1);
      },
    };
    const store = configureStore(reducers(), initState, hook);
    store.dispatch(actions.marvel.getAllCharacters());
  });




  it('should load character info', async () => {
    const initState = {
      marvel: {
        character: null,
      },
    };
    const hook = {
      [GET_ONE_CHARACTER]: getState => {
        const character = getCharacter(getState());
        expect(character).toEqual(null);
      },
    };
    const store = configureStore(reducers(), initState, hook);
    store.dispatch(actions.marvel.getOnlyOneCharacter(1));
  });
});