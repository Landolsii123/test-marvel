import { compose, toPairs, reduce } from 'ramda';
import ducks from './ducks';
const actions = compose(
  reduce((acc, [name, { actions }]) => (actions ? { ...acc, [name]: actions } : acc), {}),
  toPairs,
)(ducks);
export default actions;
