import axiosMock from 'axios';
import { getCharacters, getOneCharacter } from '../marvelService';
jest.mock('axios', () => ({
  get: jest.fn(),
}));
describe('get All Characters', () => {
  axiosMock.get.mockResolvedValueOnce({ data: { data: { results: [] } } });
  it('should return empty array', () => {
    return getCharacters().then(res => {
      const characters = res.data.results;
      expect(characters.length).toEqual(0);
    });
  });
});
describe('fetchCharacterInfo', () => {
  axiosMock.get.mockResolvedValueOnce({ data: { data: { results: [] } } });
  it('should return empty array', () => {
    return getOneCharacter().then(res => {
      const character = res.data.results;
      expect(character.length).toEqual(0);
    });
  });
});













