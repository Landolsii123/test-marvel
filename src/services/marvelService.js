import { prop } from 'ramda';

const axios = require('axios');
var crypto = require('crypto');
const BASE_URL = 'http://gateway.marvel.com:80/v1/public/characters';

const timestamp = [Math.round(+new Date() / 1000)];
const PRIVATE_API = 'aa38ec5599750c2cf801a4db9d79445470e9ab6d';
const PUBLIC_API = '1d85d702c65aba756ff90a8e19f7d2db';

const concatenatedString = timestamp.concat(PRIVATE_API, PUBLIC_API).join('');
const hash = crypto.createHash('md5').update(`${concatenatedString}`).digest('hex');
export const getCharacters = () => axios
  .get(BASE_URL, {
    params: {
      ts: timestamp,
      apikey: PUBLIC_API,
      hash,
    },
  })
  .then(prop('data'));

export const getOneCharacter = id => axios
  .get(BASE_URL+'/'+id, {
    params: {
      ts: timestamp,
      apikey: PUBLIC_API,
      hash,
    },
  })
  .then(prop('data'));