import { prop } from 'ramda';
import { createSelector } from 'reselect';
const root = prop('marvel');
export const getCharacters = createSelector(root, prop('characters'));
export const getCharacter = createSelector(root, prop('character'));
