import React, { useEffect } from 'react';
import { Button, CardMedia, Grid, makeStyles } from '@material-ui/core';
import CharacterTitle from './CharacterName';
import { useLocation } from 'react-router-dom';
import {useMarvel} from '../App/hooks';
import CharacterComics from './CharacterComics';
import CharacterSeries from './CharacterSeries';
import { useHistory } from "react-router-dom";
const useStyles = makeStyles((theme) => ({
  media: {
    height: "300px",
    width: '200px',
    //paddingTop: '56.25%', // 16:9
  },
  root:{
    padding:theme.spacing(5),
    background:'white',
    minHeight:'100vh'
  },
  details:{
    marginLeft:theme.spacing(5),
  }
}));

const CharacterDetails = () => {
  const classes = useStyles();
  const { getOnlyOneCharacter, character } = useMarvel();
  const location = useLocation();
  const id = location.pathname.slice(1);
  let history = useHistory();
  useEffect(()=>{
    getOnlyOneCharacter(id)
  },[])

  console.log('',character);
  return (
    <Grid container justify="center" className={classes.root} >
      <Grid item>
        {
          character?.thumbnail?
          <CardMedia
              className={classes.media}
              image={character.thumbnail.path+'.'+character.thumbnail.extension}
              title={character.name}
          />
          :
          <></> 
        }
      </Grid>
      <Grid item container className={classes.details} md={8} justify="space-between">
        <Grid item><CharacterTitle name={character?.name}/></Grid>
        <Grid item><Button onClick={()=>history.push(`/`)}>Go back</Button></Grid>
        <CharacterComics comics={character?.comics} />
        <CharacterSeries series={character?.series} />
      </Grid>      
        
    </Grid>
  );
};

export default CharacterDetails;