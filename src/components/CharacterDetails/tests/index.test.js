import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import configureStore from '../../../store';
import { Provider } from 'react-redux';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

describe('components | CharacterDetails | index', () => {
  it('should render CharacterDetails', () => {
    const store = configureStore();
    const history = createMemoryHistory()
    const { container } = render(
        <Provider store={store}>
            <Router history={history}>
                <Component />
            </Router>
        test </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});