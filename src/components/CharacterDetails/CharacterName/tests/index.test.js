import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';

describe('components | CharacterName| index', () => {
  it('should render CharacterName', () => {
    const name='Pechtri'
    const { container } = render(
      <Component name={name}/>
    );
    expect(container).toMatchSnapshot();
  });
});