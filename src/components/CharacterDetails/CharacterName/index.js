import React from 'react';
import { Grid, makeStyles, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
const useStyles = makeStyles(()=>({
  title:{
    fontSize:"32px",
  }
}))

const CharacterName = ({name}) => {
  const classes = useStyles()
  return (
    <Grid align="center">
      <Typography className={classes.title}>{name}</Typography>
    </Grid>
  );
};
CharacterName.propTypes={
  name:PropTypes.string
}
export default CharacterName;
