import React from 'react';
import { Grid, makeStyles, Typography, Divider } from '@material-ui/core';

import PropTypes from 'prop-types';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

const CharacterSeries = ({series}) => {
  const classes = useStyles();
  return (
    <Grid container spacing={2}>
        <Grid item xs={12} >
          <Typography variant="h6" className={classes.title}>
            Series
          </Typography>
          <Grid className={classes.demo}>
              {
                series?.items.map((item,index)=><Grid key={index}>
                <Typography>{item.name}</Typography>
                  <Divider/>
                </Grid>
                )
              }
          </Grid>
        </Grid>
    </Grid>
  );
};

CharacterSeries.propTypes={
  series:PropTypes.object
}

export default CharacterSeries;