import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import { fakeData } from '../../../../ducks/fakeData';

describe('components | CharacterSeries | index', () => {
  it('should render CharacteCharacterSeriesrComics', () => {
    const series=fakeData.data.results[0].series
    const { container } = render(
            <Component series={series}/>
    );
    expect(container).toMatchSnapshot();
  });
});