import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import { fakeData } from '../../../../ducks/fakeData';

describe('components | CharacterComics | index', () => {
  it('should render CharacterComics', () => {
    const comics=fakeData.data.results[0].comics
    const { container } = render(
            <Component comics={comics}/>
    );
    expect(container).toMatchSnapshot();
  });
});