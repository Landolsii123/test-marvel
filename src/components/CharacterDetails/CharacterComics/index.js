import React from 'react';
import { Divider, Grid, makeStyles, Typography } from '@material-ui/core';
import PropTypes from 'prop-types'


const useStyles = makeStyles((theme) => ({
  
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));


const CharacterComics = ({comics}) => {
  const classes = useStyles();
  return (
    <Grid container spacing={10}>
        <Grid item xs={12} >
          <Typography variant="h6" className={classes.title}>
            Comics
          </Typography>
          <Grid className={classes.demo}>
              {
                comics?.items.map((item,index)=><Grid key={index}>
                  <Typography >{item.name}</Typography>
                  <Divider/>
                </Grid>
                )
              }
            
          </Grid>
        </Grid>
    </Grid>
  );
};
CharacterComics.propTypes={
  comics:PropTypes.object
}
export default CharacterComics;