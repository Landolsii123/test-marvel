import { useDispatch, useSelector } from 'react-redux';
import { getAllCharacters, getOnlyOneCharacter } from '../../ducks/marvel';
import { getCharacter, getCharacters } from '../../selectors/marvel';

export const useMarvel = () => {
  const dispatch = useDispatch();
  return {
    getAllCharacters: () => dispatch(getAllCharacters()),
    getOnlyOneCharacter: index => dispatch(getOnlyOneCharacter(index)),
    characters: useSelector(getCharacters),
    character: useSelector(getCharacter),
  };
};
