import React from 'react';
import { Grid } from '@material-ui/core';
import loadable from '@loadable/component';
import Loader from '../../layout/Loader';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import NotFound from '../../pages/404page'
const LoadableCharacters = loadable(() => import('../Characters/index'), {
  fallback: <Loader />,
});
const LoadableCharacterDetails = loadable(() => import('../CharacterDetails/index'), {
  fallback: <Loader />,
});


const App = () => {
  return (
    <Grid>
        <Router>
          <Switch>
            <Route component={LoadableCharacters} exact path="/" />
            <Route component={LoadableCharacterDetails} exact path="/:id" />
            <Route component={NotFound} path="*" />
          </Switch>
      </Router>
    </Grid>
  );
};

export default App;
