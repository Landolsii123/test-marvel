import React from 'react';
import { Grid } from '@material-ui/core';
import Title from './Title';
import CharactersList from './CharactersList';

const Characters = () => {
  return (
    <Grid>
        <Title />
        <CharactersList/>
    </Grid>
  );
};

export default Characters;
