import React from 'react';
import { Avatar, Box, Card, CardActions, CardHeader, CardMedia, Grid, IconButton, makeStyles } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { red } from '@material-ui/core/colors';
import PropTypes from 'prop-types'
import { useHistory } from "react-router-dom";
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import LastPageIcon from '@material-ui/icons/LastPage';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    marginTop:theme.spacing(2)
  },
  header:{
    cursor:'pointer'
  },
  root: {
    maxWidth: 345,
    minWidth: 345
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));
//NZCJNR-BMQUH2-54LASW
const Character = ({character}) => {
  let history = useHistory();
  const classes = useStyles()
  return (
        <Grid item md={4} className={classes.wrapper} component={Box}>
          <Card className={classes.root}>
            <CardHeader
              className={classes.header}
              onClick={()=>{history.push(`/${character.id}`)}}
              avatar={
                <Avatar aria-label="avatar" className={classes.avatar}>
                  {character.name.charAt(0)}
                </Avatar>
              }
              action={
                <IconButton aria-label="settings">
                  <MoreVertIcon />
                </IconButton>
              }
              title={character.name}
              subheader={'Id : ' +character.id}
            />
            <CardMedia
              className={classes.media}
              image={character.thumbnail.path+'.'+character.thumbnail.extension}
              title={character.name}
            />
            <CardActions disableSpacing>
              <IconButton aria-label="add to favorites" href={character?.urls[0].url} target="_blank">
                <MoreHorizIcon />
              </IconButton>details
              <IconButton aria-label="share" href={character?.urls[1].url} target="_blank">
                <LastPageIcon />
              </IconButton>link
            </CardActions>
          </Card>
        </Grid>
  );
};

Character.propTypes={
  character:PropTypes.object
}
export default Character;
