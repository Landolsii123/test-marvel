import React from 'react';
import { Grid, makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme=>({
  title:{
    fontSize:"32px",
    marginTop:theme.spacing(4)
  }
}))

const Title = () => {
  const classes = useStyles()
  return (
    <Grid align="center">
      <Typography className={classes.title}>Marvel Characters</Typography>
    </Grid>
  );
};

export default Title;
