import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';

describe('components | Title | index', () => {
  it('should render Title', () => {
    const { container } = render(
            <Component />
    );
    expect(container).toMatchSnapshot();
  });
});