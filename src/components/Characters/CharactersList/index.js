import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';
import {useMarvel} from '../../App/hooks'
import Character from '../Character';

const CharactersList = () => {
  const { getAllCharacters, characters } = useMarvel();
  useEffect(()=>{
    getAllCharacters()
  },[])
  //console.log(characters);
  
  return <Grid container align="center" justify="space-around"> 
        {
          characters?.map((character,index)=><Character character={character} key={index} />)
        }
    </Grid>
  
};

export default CharactersList;
