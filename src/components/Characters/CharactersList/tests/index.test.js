import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import configureStore from '../../../../store';
import { Provider } from 'react-redux';

describe('components | CharactersList | index', () => {
  it('should render CharactersList', () => {
    const store = configureStore();
    const { container } = render(
      <Provider store={store}>
        <Component />
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});