import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import App from './components/App';
import { Provider } from 'react-redux';
import configureStore from './store';

const store = configureStore();

const theme = createMuiTheme();

const ROOT = (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </Provider>
);
ReactDOM.render(ROOT, document.getElementById('root'));
